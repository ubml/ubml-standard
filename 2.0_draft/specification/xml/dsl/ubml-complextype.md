## ComplexType

复杂类型是没有唯一标识的标准的结构化类型。由于复杂类型没有唯一标识，复杂类型不能脱离实体类型进行引用、创建、更新、删除等操作。复杂类型用来对属性进行分组。

复杂类型元素必须包含名称（Name）属性。
复杂类型元素可以包含多个Property属性，在一个复杂类型中，Property元素名称不允许重复。

```
<ComplexType Name="Header">
  <Property Name="ID" Type="Bdm.String" Nullable="false"/>
  <Property Name="Code" Type="Bdm.String" Nullable="false"/>
  <Property Name="Name" Type="Bdm.String" Nullable="true"/>
  <Property Name="Namespace" Type="Bdm.String" Nullable="false"/>
  <Property Name="Type" Type="Bdm.String" Nullable="false"/>
  <Property Name="Language" Type="Bdm.String" Nullable="true"/>
  <Property Name="Translated" Type="Bdm.Boolean" Nullable="true"/>
  <Property Name="Extendable" Type="Bdm.Boolean" Nullable="true"/>
</ComplexType>
```