## Property

属性是指结构化属性，属性可以是以下类型：

+ PrimitiveType
+ ComplexType
+ EnumerationType
+ 以上类型的集合

一个结构化属性，必须指定一个名称与类型。
属性名称在使用属性的结构化类型中，必须唯一。
Property本身必须包含名称（Name）与类型（Type）属性，可以包含额外的描述属性，比如：Nullable, DefaultValue.

```
<ComplexType Name="Header">
  <Property Name="ID" Type="Bdm.String" Nullable="false"/>
  <Property Name="Code" Type="Bdm.String" Nullable="false"/>
  <Property Name="Name" Type="Bdm.String" Nullable="true"/>
  <Property Name="Namespace" Type="Bdm.String" Nullable="false"/>
  <Property Name="Type" Type="Bdm.String" Nullable="false"/>
  <Property Name="Language" Type="Bdm.String" Nullable="true"/>
  <Property Name="Translated" Type="Bdm.Boolean" Nullable="true"/>
  <Property Name="Extendable" Type="Bdm.Boolean" Nullable="true"/>
</ComplexType>
```

