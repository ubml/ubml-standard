## EntityType

实体类型是标准的结构化类型，实体类型代表了具体的实体，任何具有唯一标识的实体都可以用实体类型表述，比如客户，订单。
实体类型必须包含名称（Name）属性。
实体类型必须包含Key元素，用于指定唯一标识属性。
实体类型可以包含多个Property元素，在一个实体类型中，Property元素名称不允许重复。


```
<EntityType Name="Product">
  <key>
    <PropertyRef>ID</PropertyRef>
  </key>
  <Property Name="ID" Type="Bdm.String" Nullable="fales"/>
  <Property Name="Code" Type="Bdm.String" Nullable="false"/>
</EntityType>
```

```
<Bdm Version="0.1.0">
  <Model Name="SalesOrder" Type="BusinessEntity">
    <ComplexType Name="Header">
      <Property Name="ID" Type="Bdm.String" Nullable="false"/>
      <Property Name="Code" Type="Bdm.String" Nullable="false"/>
      <Property Name="Name" Type="Bdm.String" Nullable="true"/>
      <Property Name="Namespace" Type="Bdm.String" Nullable="false"/>
      <Property Name="Type" Type="Bdm.String" Nullable="false"/>
      <Property Name="Language" Type="Bdm.String" Nullable="true"/>
      <Property Name="Translated" Type="Bdm.Boolean" Nullable="true"/>
      <Property Name="Extendable" Type="Bdm.Boolean" Nullable="true"/>
    </ComplexType>
  </Model>
</Bdm>
  
```