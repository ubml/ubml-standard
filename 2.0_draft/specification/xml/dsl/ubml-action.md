## Action无返回值函数
无返回值函数是定义服务的一种操作，能够具有明显的可观察效果，可以返回单个实例或任何类型的实例的集合。

无返回值函数的名称是一个简单标识符，在一份元模型结构描述中必须唯一。

无返回值函数可以指定一个返回类型，该返回类型必须是基本类型、实体类型或复杂类型，或者是基本类型、实体类型或复杂类型的集合。

无返回值函数可以定义在操作执行过程中使用的参数。

```
<Action Name="save">
  <Parameter Name="salesOrderEntity" Type="SalesOrder"/>
  <Parameter Name="context" Type="BusinessContext"/>
</Action>

```


