module.exports = {
  base: "/ubml-standard",
  title: "UBML",
  description: "统一业务建模语言",
  themeConfig: {
    sidebar: [
      {
        text: '指南',
        items: [
          { text: 'UBML简介', link: '/guide/overview' },
          { text: '设计策略', link: '/guide/design_strategy' },
          { text: 'FAQ', link:'/guide/faq.md'}
        ]
      }
    ]
  }
};
