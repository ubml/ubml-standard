---
layout: home
hero:
  name: UBML
  text: 统一业务建模语言
  tagline: 一种基于领域特定语言，用于快速构建应用软件的，低代码建模语言。
  actions:
    - theme: brand
      text: 了解 UBML
      link: /guide/overview
    - theme: Gitee
      text: View on Gitee
      link: https://gitee.com/ubml/ubml-standard
---